# CoffeeMachineProject

Environnement de travail
 -java  jdk 17
 -IDE intellJ qui supporte également eclipse
 -Maven 3.8
 -junit5 5.9.1

Méthode de Trevaille
 TDD : Test Driven Developpement qui est une méthode de développement pilotée par les tests. 

## Name
kata Coffee Machine

## Description
Le kata coffe Machine est un exercice pour développeur dont l'objectif est de mettre sur pied une solution qui permet de ressortir la capacité technique de l'ingénieur (le développeur), tant sur ses compétences de maitrise du langage que sur ses aptitudes à analyser, modéliser, implémenter. On doit pouvoir retrouver les bonnes pratiques par exemple dans le code.
Pour le cas échéant, le langage de programmation est java, l'IDE intellJ, pour le dépot gitlab.

La machine à café produit une boisson lorsqu'on lui fournit un protocole logique suivant la commande passée par un client. Une commande est constituée d'une boisson sélectionnée, du nombre de sucre, d'un montant et d'une option de boisson chaude ou pas. L'exercice est subdivisé en quatre parties nommées par : Itération 1, Itération 2, Itération 3 et Itération 4. L'approche conseillée par le kata est le TDD qui permet de balayer un large spectre de cas de figure.

Pour ce faire bien évidemment j'ai utilisé l'approche TDD dans le test unitaire nommé MakeLogicCoffeeMachineTest qui est la classe test du service MakeLogicCoffeeMachine. Déjà MakeLogicCoffeeMachineTest a été compartimenté pour une meilleure organisation, en 4 partie avec l'annotation @Nested. Les tests concernant l'Itération 1 se trouvent dans la classe MakeDrinkProtocol, l'Itération 2 dans la classe ValidateAmountDrinkProtocol, l'Itération 3 dans la classe IntegrateExtraHotDrinkProtocol et l'Itération 4 dans la classe MakeReportDrink.


Itération 1 / MakeDrinkProtocol : En input on a boisson, sucre et en output on a un protocole ou une erreur
  pour le tout premier test par exemple shouldReturnErrorWhenUnknownDrink, j'ai débuté avec le cas pour lequel un bug a généré une boisson et doit retourner une erreur, nous avons balayé ensuite tous les cas de combinaison BOISSON:NOMBRE_SUCRE:AVEC_TOUILLET.

Itération 2 / ValidateAmountDrinkProtocol : En input on a boisson, sucre, montant et en output on a un protocole ou une erreur de mauvais montant 
  pour le tout premier test à savoir shouldReturnTeaWithoutSugarWithLessAmount qui réalise le test selon lequel la boisson est le thé sans sucre et un montant inférieur à celui qui correspond au prix d'une tasse de thé.

Itération 3 / IntegrateExtraHotDrinkProtocol : En input boisson, sucre, montant, boisson_chaude et en output on a un protocole
  pour le tout premier test qui est shouldReturnOrangeJuiceWithValidAmount qui réalise le test selon lequel le jus d'orange avec un montant valide est sélectionné.

Itération 4 / MakeReportDrink : En input on a une ou plusieurs commande(s) et en output on a le nombre de boissons vendu par type et le montant total des ventes de toutes les boissons
  pour le tout premier test  qui est shouldReturnNoSumAndEmptyReport, il permet d'afficher un montant total de vente de 0 euro pour aucune boisson vendue.

Ces tests ont permis de générer :
  -la classe DrinkOrder dont l'un des champs est de type enum drink qui comporte deux champs : codeDrink(pour le code boisson, par exemple "H" pour chocolat) et montantDrink (pour le montant de la boisson, par exemple 0.5 pour le chocolat)
  -la methode getOrderCoffeeMachine qui prend en paramètre la boisson, le nombre de sucre, le montant et le boolean boisson extra hot, cette méthode nous renvoie le protocle que la machine attend pour produire la boisson.